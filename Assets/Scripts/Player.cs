using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

	public Text healthText;

	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;
	private EdgeCollider2D collisionLayer;
	private int playerHealth = 6;

	[SerializeField]
	private float movementSpeed;

	private bool facingRight;

	// Use this for initialization
	protected override void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
		rigidBody = GetComponent<Rigidbody2D> ();
		collisionLayer = GetComponent<EdgeCollider2D>();
		facingRight = true;
		base.Start ();
		healthText.text = "Health: " + playerHealth;
	}

	// Update is called once per frame
	void FixedUpdate () {

		float horizontal = Input.GetAxis ("Horizontal");

		HandleMovement (horizontal);

		Flip (horizontal);
	}

	private void HandleMovement(float horizontal) {
		rigidBody.velocity = new Vector2(horizontal * movementSpeed, rigidBody.velocity.y);
	}

	private void Flip(float horizontal){
		if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) {
			facingRight = !facingRight;

			Vector3 theScale = transform.localScale;

			theScale.x *= -1;

			transform.localScale = theScale;
		}
	}
}